/**
 * LogReserva.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    accion: {
      type: "string",
      columnType: "varchar",

    },
    anterior: {
      type: "string",
      columnType: "varchar",

    },
    actual: {
      type: "string",
      columnType: "varchar",

    },
    idReserva: {
      type: "number",
      columnType: "int",
    },
    idUsuario: {
      type: "number",
      columnType: "int",
    }
    
  },

};

