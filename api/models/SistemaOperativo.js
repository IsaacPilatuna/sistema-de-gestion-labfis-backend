/**
 * SistemaOperativo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nombre: {
      type: "string",
      columnType: "varchar",
     
    },
    estado: {
      type: "string",
      columnType: "varchar",
      maxLength: 10,
      isIn:['ACTIVO','INACTIVO'],
      defaultsTo:'ACTIVO'
  },
  arregloOrdenadorSistemaOperativo: {
      collection: 'OrdenadorSistemaOperativo',
      via: 'fkSistemaOperativo'
    }
  
  },

};

