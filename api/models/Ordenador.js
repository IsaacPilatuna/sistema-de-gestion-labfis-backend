/**
 * Ordenador.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    ram: {
      type: "number",
      columnType: "int",
    },
    fkProcesador: {
      model: "Procesador"
    },
   
    fkLaboratorio: {
      model: "laboratorio"
    },
  
   
    arregloOrdenadorSistemaOperativo: {
      collection: 'OrdenadorSistemaOperativo',
      via: 'fkOrdenador'
    }

  },

};

