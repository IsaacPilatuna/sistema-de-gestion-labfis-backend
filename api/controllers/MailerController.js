const nodemailer = require('nodemailer');
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user:"sislabfis@gmail.com",
        pass:"adminLABFIS2020*"
    },
    tls: { rejectUnauthorized: false }
});
module.exports = {
  
    enviarCorreo: async(req,res)=>{
        const parametros=req.allParams();
        const asunto=parametros.asunto;
        const destinatario=parametros.destinatario;
        const htmlMessage=parametros.htmlMessage

        const message = {
            from: 'sislabfis@gmail.com', 
            to: destinatario,         
            subject: asunto, 
            text:'',
            html: htmlMessage
           };
        transporter.sendMail(message, function(err, info) {
            if (err) {
                return res.serverError({
                    error:500,
                    mensaje:'Error del servidor: '+err
                });
            } else {
              return res.ok({
                  mensaje: 'Exito',
                  info: info
              })
            }
        });

    },
    enviarCorreoRecuperacion(correo,token){
        const message = {
            from: 'sislabfis@gmail.com', 
            to: correo,         
            subject: "Solicitud de Recuperación de Contraseña", 
            text:'',
            html: "<h3>Recuperación de contraseña</h3><p>Estimado Usuario</p><p>Se solicitó recuperar la contraseña para su cuenta en el Sistema de Gestión de Laboratorios - LABFIS</p><a href='https://labfis.web.app/#/recuperacion-password?token="+token+"' target='_blank'>Click para recuperar contraseña</a><p>Si usted no solicitó recuperar la contraseña, por favor ignore este correo electrónico</p><br><br><img width='250' src='https://firebasestorage.googleapis.com/v0/b/sistema-de-gestion-labfis.appspot.com/o/labfis-logo.png?alt=media&token=cf508534-e602-409f-b9e2-071fc0733786' alt='Logo LABFIS'>"
           };
        transporter.sendMail(message, function(err, info) {
            if (err) {
                console.log(err)
            } else {
              console.log(info)
            }
        });
    }

};

