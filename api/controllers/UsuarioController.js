
const MailerController = require("./MailerController");
const { v4: uuid } = require('uuid');

module.exports = {
    
    iniciarSesion:async(req,res)=>{
        const parametros=req.allParams();
        const password=parametros.password;
        const correo=parametros.correo;
        var loginResult=false;
        var userResult={};
        try {
            var resultadoCorreo=await Persona.find({correo:correo});
            if(resultadoCorreo.length>0){
                var persona = resultadoCorreo[0];
                var resultadoFkTipo=await Tipopersona.find({id:persona.fkTipoPersona});
                var resultadoUsuario=await Usuario.find().where({and:[{password:password},{fkPersona:persona.id}]});
                if(resultadoUsuario.length>0){
                    loginResult=true;
                    userResult={
                        idUsuario:resultadoUsuario[0].id,
                        tipoUsuario:resultadoFkTipo[0].nombre,
                        estado:resultadoUsuario[0].estado,
                        idPersona: persona.id 

                    }
                }
    
            }
            return res.ok({
                login: loginResult,
                user:userResult
            })

        } catch (error) {
            console.error(error);
            return res.serverError({
                error: 500,
                mensaje:'Error del servidor: '+error
            });
        }
    },

};