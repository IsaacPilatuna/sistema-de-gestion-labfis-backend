/**
 * Reservas
 *
 * @description :: Server-side logic for managing Reservas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {
    editarReserva:async(req,res)=>{
        const parametros=req.allParams();
        const reserva=req.body;
        const idUsuario= parametros.idUsuario
      
        try {
            
            var reservaAnterior= await Reservas.findOne({id:reserva.id});
            reserva.fkPersonaCrea = reservaAnterior.fkPersonaCrea
            reservaAnterior.fkLaboratorio = await Laboratorio.findOne({id:reservaAnterior.fkLaboratorio})
            reservaAnterior.fkPersonaReserva = await Persona.findOne({id:reservaAnterior.fkPersonaReserva})
            reservaAnterior.fkPersonaCrea = await Persona.findOne({id:reservaAnterior.fkPersonaCrea})
            reservaAnterior.fkMateria = await Materia.findOne({id:reservaAnterior.fkMateria})
            reservaAnterior.fkCiclo = await Ciclo.findOne({id:reservaAnterior.fkCiclo})
            
            let reservaModificada = await Reservas.update({ id: reserva.id }).set(reserva)
            reserva.fkLaboratorio = await Laboratorio.findOne({id:reserva.fkLaboratorio})
            reserva.fkPersonaReserva = await Persona.findOne({id:reserva.fkPersonaReserva})
            reserva.fkPersonaCrea = await Persona.findOne({id:reserva.fkPersonaCrea})
            reserva.fkMateria = await Materia.findOne({id:reserva.fkMateria})
            reserva.fkCiclo = await Ciclo.findOne({id:reserva.fkCiclo})
            
            await LogReserva.create({
                idReserva: reserva.id,
                accion: "MODIFICAR",
                anterior: JSON.stringify(reservaAnterior),
                actual: JSON.stringify(reserva),
                idReserva: reserva.id,
                idUsuario: idUsuario

            })
            
            return res.ok({
                estado: true,
                mensaje: 'Registro modificado exitosamente'
            });
            
            
        } catch (error) {
            return res.serverError({
                error: 500,
                mensaje: 'Error del servidor: ' + error
            });
            
        }



    }
};