
const MailerController = require("./MailerController");
const { v4: uuid } = require('uuid');

module.exports = {
  
    solicitarRecuperacion: async (req, res) => {
        const parametros = req.allParams();
        const correo = parametros.correo;
        var respuesta=false;
        var mensaje='';
        try {
            var resultadoCorreo = await Persona.find({ correo: correo });
            if(resultadoCorreo.length>0){
                let persona=resultadoCorreo[0];
                var resultadoUsuario = await Usuario.find({fkPersona:persona.id});
                if(resultadoUsuario.length>0){
                    let usuario=resultadoUsuario[0];
                    var recuperacion = await RecuperacionCuenta.create({token:uuid(),fkUsuario:usuario.id}).fetch();
                    await MailerController.enviarCorreoRecuperacion(correo,recuperacion.token);
                    respuesta=true;
                    mensaje="Correo electrónico enviado";
                }else{
                    mensaje="El usuario aún no ha sido registrado en el sistema, porfavor realizar registro";
                }
            }else{
                mensaje="El correo electrónico no se encuentra registrado en el sistema";
            }
            return res.ok({
                estado: respuesta,
                mensaje: mensaje
            });

        } catch (error) {

            console.error(error);
            return res.serverError({
                error: 500,
                mensaje: 'Error del servidor: ' + error
            });
        }

    },
    updatePassword:async(req,res)=>{
        const parametros = req.allParams();
        const token = parametros.token;
        const password = parametros.password;
        var respuesta=false;
        var mensaje='';
        try {
            var resultadoRecuperacion = await RecuperacionCuenta.find({token:token});
            if(resultadoRecuperacion.length>0){
                await Usuario.update(resultadoRecuperacion[0].fkUsuario,{password:password});
                await RecuperacionCuenta.destroy([resultadoRecuperacion[0].id]);
                respuesta=true;
                mensaje="Contraseña actualizada exitosamente";
            }else{
                mensaje="La solicitud de cambio de contraseña ha expirado";
            }
            return res.ok({
                estado: respuesta,
                mensaje: mensaje
            });
            
        } catch (error) {
            
            console.error(error);
            return res.serverError({
                error: 500,
                mensaje: 'Error del servidor: ' + error
            });
        }

    }

};

