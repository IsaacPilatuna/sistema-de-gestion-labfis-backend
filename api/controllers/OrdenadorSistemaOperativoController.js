/**
 * OrdenadorSistemaOperativoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    registrarSistemasOperativos:async(req,res)=>{
        const parametros=req.allParams();
        const idOrdenador=parametros.idOrdenador;
        const sistemasOperativosOrdenador=req.body;
        debugger
        try {
            if(sistemasOperativosOrdenador.length>0){
                await OrdenadorSistemaOperativo.destroy({fkOrdenador:idOrdenador});
                
                sistemasOperativosOrdenador.forEach(async (id) => {
                    await OrdenadorSistemaOperativo.create({fkOrdenador:idOrdenador,fkSistemaOperativo:id});
                });
                debugger
            return res.ok({
                estado: true,
                mensaje: ''+sistemasOperativosOrdenador.length+'Registros realizados exitosamente'
            });
            }
            
        } catch (error) {
            return res.serverError({
                error: 500,
                mensaje: 'Error del servidor: ' + error
            });
            
        }



    }

};

